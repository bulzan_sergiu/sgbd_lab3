--TASK 1 

use CatalogScolar
go 

DROP PROC addElevParinte
go 
CREATE PROC addElevParinte( @prenumeElev NVARCHAR(50), @numeElev NVARCHAR(50), @genreElev NVARCHAR(10), @dataNasterii DATE, @telefonElev CHAR(10), @emailElev NVARCHAR(50), @idClasa SMALLINT, 
						@prenumeParinte NVARCHAR(50), @numeParinte NVARCHAR(50), @telefonParinte CHAR(10), @emailParinte NVARCHAR(50))
AS
BEGIN 
	BEGIN TRAN
		BEGIN TRY
			IF (dbo.validateGenre(@genreElev) <> 1)
			BEGIN
				RAISERROR('Gen-ul elevului este invalid. Trebuie sa fie masculin/feminin',14,1); 
			END 
			IF (dbo.validatePhone(@telefonElev) <> 1)
			BEGIN
				RAISERROR('Telefonul elevului este invalid',14,1); 
			END
			IF (dbo.validateEmail(@emailElev) <> 1)
			BEGIN
				RAISERROR('Email-ul elevului este invalid',14,1); 
			END 
			IF (dbo.validateClassId(@idClasa) <> 1) 
			BEGIN
				RAISERROR('ID-ul de clasa a elevului este invalid',14,1); 
			END 

			INSERT INTO Elev VALUES(@prenumeElev,@numeElev, @genreElev, @dataNasterii, @telefonElev, @emailElev, @idClasa) 
			DECLARE @insertedElevId INT = SCOPE_IDENTITY(); 

			IF (dbo.validatePhone(@telefonParinte) <> 1) 
			BEGIN
				RAISERROR('Telefonul parintelui este invalid',14,1); 
			END 
			IF (dbo.validateEmail(@emailParinte) <> 1) 
			BEGIN
				RAISERROR('Email-ul parintelui este invalid',14,1); 
			END 

			INSERT INTO Parinte VALUES(@prenumeParinte, @numeParinte, @telefonParinte, @emailParinte); 
			DECLARE @insertedParinteId INT = SCOPE_IDENTITY(); 

			INSERT INTO Elev_Parinte values(@insertedElevId, @insertedParinteId); 

			COMMIT TRAN
			SELECT 'Transaction committed'
		END TRY
		BEGIN CATCH
			ROLLBACK TRAN
			SELECT 'Transaction rolledback'
			SELECT ERROR_MESSAGE() 
		END CATCH
END
GO

EXEC addElevParinte 'Flaviu','Popp','Masculin', '1996-01-03', '0747182822', 'flaviu@pop.com', 3, 'Parinte', 'Popescu', '0743123823', 'parinte@popescu.com' --works
EXEC addElevParinte 'Flaviu','Popp2','Masculin', '1996-01-03', '0747182822', 'flaviu@pop.com', 3, 'Parinte', 'Popescu', '0743123823', 'parintepopescu.com' --invalid parent email, no student is saved


SELECT * 
FROM ELEV
INNER JOIN Elev_Parinte ON
Elev.IDElev = Elev_Parinte.IDElev
INNER JOIN Parinte ON
Elev_Parinte.IDParinte = Parinte.IDParinte

