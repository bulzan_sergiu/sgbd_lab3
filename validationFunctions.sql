--Laborator 3

use CatalogScolar
go

-- Validate Genre function 
drop function dbo.validateGenre
go
CREATE FUNCTION validateGenre (@genre NVARCHAR(10)) 
RETURNS BIT AS
BEGIN
	DECLARE @ret as BIT; 
	if ( @genre <> '' AND ( LOWER(@genre) = 'masculin' OR LOWER(@genre) = 'feminin'))
		SET @ret =  1; 
	else
		SET @ret = 0; 
	RETURN @ret
END 

-- Valide Email function 
drop function dbo.validateEmail
go
CREATE FUNCTION validateEmail (@Email NVARCHAR(50))
returns BIT
AS
BEGIN
	DECLARE @ret BIT
	if( @Email NOT LIKE '%_@__%.__%' )
		set @ret = 0 
	else
		set @ret = 1
	return @ret
END


-- Validate phone function 
drop function dbo.validatePhone
go
CREATE FUNCTION validatePhone(@Phone NVARCHAR(50))
returns BIT
AS
BEGIN
	DECLARE @ret BIT
	if( @Phone LIKE '0%' and LEN(@Phone) = 10 )
		set @ret = 1
	else
		set @ret = 0
	return @ret
END


--Validate Class Id 
drop function dbo.validateClassId;
go
CREATE FUNCTION validateClassId(@classId Int)
returns BIT
AS
BEGIN
	DECLARE @ret BIT
	IF EXISTS ( select c.IDClasa from Clasa c where c.IDClasa = @classId )
		set @ret = 1
	else
		set @ret = 0
	return @ret
END
