use CatalogScolar
go 

DROP PROC addElevParinteTask2
go 
CREATE PROC addElevParinteTask2( @prenumeElev NVARCHAR(50), @numeElev NVARCHAR(50), @genreElev NVARCHAR(10), @dataNasterii DATE, @telefonElev CHAR(10), @emailElev NVARCHAR(50), @idClasa SMALLINT, 
						@prenumeParinte NVARCHAR(50), @numeParinte NVARCHAR(50), @telefonParinte CHAR(10), @emailParinte NVARCHAR(50))
AS
BEGIN 
	BEGIN TRAN
		BEGIN TRY
			DECLARE @insertedElevId INT = 0 
			IF (dbo.validateGenre(@genreElev) <> 1)
			BEGIN
				RAISERROR('Gen-ul elevului este invalid. Trebuie sa fie masculin/feminin',14,1); 
			END 
			IF (dbo.validatePhone(@telefonElev) <> 1)
			BEGIN
				RAISERROR('Telefonul elevului este invalid',14,1); 
			END
			IF (dbo.validateEmail(@emailElev) <> 1)
			BEGIN
				RAISERROR('Email-ul elevului este invalid',14,1); 
			END 
			IF (dbo.validateClassId(@idClasa) <> 1) 
			BEGIN
				RAISERROR('ID-ul de clasa a elevului este invalid',14,1); 
			END 

			INSERT INTO Elev VALUES(@prenumeElev,@numeElev, @genreElev, @dataNasterii, @telefonElev, @emailElev, @idClasa) 
			SET @insertedElevId  = SCOPE_IDENTITY(); 

			COMMIT TRAN 
			SELECT 'Tranzactie elev efectuata cu succes'
		END TRY
		BEGIN CATCH
			ROLLBACK TRAN 
			SELECT 'Tranzactie elev fara success'
			SELECT ERROR_MESSAGE() 
		END CATCH 

	BEGIN TRAN
		BEGIN TRY 
			DECLARE @insertedParinteId INT = 0 
			IF (dbo.validatePhone(@telefonParinte) <> 1) 
			BEGIN
				RAISERROR('Telefonul parintelui este invalid',14,1); 
			END 
			IF (dbo.validateEmail(@emailParinte) <> 1) 
			BEGIN
				RAISERROR('Email-ul parintelui este invalid',14,1); 
			END 

			INSERT INTO Parinte VALUES(@prenumeParinte, @numeParinte, @telefonParinte, @emailParinte); 
			SET @insertedParinteId  = SCOPE_IDENTITY(); 

			COMMIT TRAN
			SELECT 'Tranzactie adaugare parinte efectuata cu succes'
		END TRY
		BEGIN CATCH 
			ROLLBACK TRAN 
			SELECT 'Tranzactie eadaugare parinte fara success'
			SELECT ERROR_MESSAGE() 
		END CATCH 

	BEGIN TRAN
		BEGIN TRY 
			IF NOT(@insertedElevId > 0  AND @insertedParinteId > 0)
			BEGIN
				RAISERROR('Invalid id', 14, 1)
			END 

			INSERT INTO Elev_Parinte values(@insertedElevId, @insertedParinteId); 

			COMMIT TRAN
			SELECT 'Transaction Elev_parinte committed'
		END TRY
		BEGIN CATCH
			ROLLBACK TRAN
			SELECT 'Transaction elev_parinte rolledback'
			SELECT ERROR_MESSAGE() 
		END CATCH
END
GO

--case 1 : invalid parent email. PARINTE = NOT SAVED, ELEV = SAVED, ELEV_PARINTE = NOT SAVED 
EXEC addElevParinteTask2 'Flaviu','Popp3','Masculin', '1996-01-03', '0747182822', 'flaviu@pop.com', 3, 'Parinte', 'Popescu', '0743123823', 'parintepopescu.com'  

--case 2 : invalid student email. PARINTE = SAVED, ELEV = NOT SAVED, ELEV_PARINTE = NOT SAVED 
EXEC addElevParinteTask2 'Flaviu','Popp4','Masculin', '1996-01-03', '0747182822', 'flaviupop.com', 3, 'Parinte', 'Popescu', '0743123823', 'parinte@popescu.com' 

--case 3 : everything is ok, all info is saved
EXEC addElevParinteTask2 'Flaviu','Popp5','Masculin', '1996-01-03', '0747182822', 'flaviu@pop.com', 3, 'Parinte', 'Popescu', '0743123823', 'parinte@popescu.com' 

--case 4 : invalid student and parent email, nothing is saved 
EXEC addElevParinteTask2 'Flaviu','Popp5','Masculin', '1996-01-03', '0747182822', 'flaviupop.com', 3, 'Parinte', 'Popescu', '0743123823', 'parintepopescu.com' 