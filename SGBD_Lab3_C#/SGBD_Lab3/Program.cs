﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SGBD_Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlConnection con = new SqlConnection("Data Source=SERGIUBULZA76E6\\SQLEXPRESS; Initial Catalog = CatalogScolar; Integrated Security = true");
            con.Open();

            Thread t1 = new Thread(() => run_transaction(con, "DeadT1"));
            Thread t2 = new Thread(() => run_transaction(con, "DeadT2"));
            t1.Start();
            t2.Start();
            t1.Join();
            t2.Join();
            Console.WriteLine("All threads complete");
            con.Close();
            Console.ReadLine();
        }

        static void run_transaction(SqlConnection con, String storedProcName)
        {
            int retries = 5;
            while (retries > 0)
            {
                try
                {
                    executeStoredProcedure(con, storedProcName);
                    Console.WriteLine(storedProcName + " finished successfully");
                    break; // success!
                }
                catch
                {
                    Console.WriteLine("retry transaction " + storedProcName);
                    retries--;
                }
            }
        }

        static void executeStoredProcedure(SqlConnection con, String storedProcName)
        {
            SqlCommand cmd = new SqlCommand(storedProcName, con);
            cmd.CommandType = CommandType.StoredProcedure;
            Int32 rowsAffected = cmd.ExecuteNonQuery();
        }

    }
}


