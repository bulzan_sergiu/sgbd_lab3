use CatalogScolar
go

--dirty read unsolved TRANSACTION 1

--        DIRTY READS	
BEGIN TRAN
UPDATE Profesor 
SET Email = 'marian.vasi@yahoo.com'
WHERE Prenume = 'Vasile'
WAITFOR DELAY '00:00:10'
ROLLBACK TRAN

--    NON-REPEATABLE READS 

SELECT * from Profesor;

/*
UPDATE Profesor
SET Email = 'marianv@yahoo.com'
WHERE Prenume = 'Vasile'
*/ -- reset 

BEGIN TRAN
WAITFOR DELAY '00:00:10'
UPDATE Profesor
SET Email = 'newVasileEmail@yahoo.com'
WHERE Prenume = 'Vasile'
COMMIT TRAN



--    PHANTOM READS 

/*
DELETE
FROM Profesor
where Prenume = 'Gelu'
*/ --reset 

SELECT * from Profesor WHERE Gen = 'Barbat'

BEGIN TRAN
WAITFOR DELAY '00:00:10'
INSERT INTO Profesor VALUES('Gelu', 'Pop', 'Barbat', '0747185822', 'gelup@yahoo.com'); 
COMMIT TRAN


--     DEADLOCK T1
select * from Elev
select * from Parinte
/*
update elev 
set prenume = 'Ionut'
where IDElev = 2 

update Parinte
set nrTel = 'tel'
where nume = 'Popescu'
*/ --reset 


-- T1 and T2 have the same cost, so the vistim is chosen randomly 

--having set priority to HIGH will asure that this transaction will complete, while the other will fail. 
SET DEADLOCK_PRIORITY HIGH;

BEGIN TRAN
UPDATE Elev
SET Prenume = 'deadlockT1'
WHERE IDElev = 2
WAITFOR DELAY '00:00:10'
UPDATE Parinte
SET nrTel = 'deadT1'
WHERE Nume = 'Popescu'
COMMIT TRAN


SELECT @@SPID AS 'ID', SYSTEM_USER AS 'Login Name', USER AS 'User Name';  
SELECT session_id,deadlock_priority FROM sys.dm_exec_sessions