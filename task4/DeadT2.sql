Create procedure DeadT2
as 
BEGIN
	BEGIN TRY
		BEGIN TRAN
		UPDATE Parinte
		SET nrTel = 'deadT2'
		WHERE Nume = 'Popescu'
		WAITFOR DELAY '00:00:10'
		UPDATE Elev
		SET Prenume = 'deadlockT2'
		WHERE IDElev = 2
		COMMIT TRAN
		SELECT 'transaction 2 has been committed'
	END TRY
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT 'transaction 2 has been chosen as deadlock victim'
		exec DeadT2
	end catch
end