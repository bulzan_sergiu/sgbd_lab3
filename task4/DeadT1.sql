Create procedure DeadT1
as 
BEGIN
	BEGIN TRY
		BEGIN TRAN
		UPDATE Elev
		SET Prenume = 'deadlockT1'
		WHERE IDElev = 2
		WAITFOR DELAY '00:00:10'
		UPDATE Parinte
		SET nrTel = 'deadT1'
		WHERE Nume = 'Popescu'
		COMMIT TRAN
		SELECT 'transaction 1 has been committed'
	END TRY
	BEGIN CATCH 
		ROLLBACK TRAN
		SELECT 'transaction 1 has been chosen as deadlock victim'
		exec DeadT1
	end catch
end