use master
IF not exists( select * from sys.databases where name = 'CatalogScolar' )
	CREATE DATABASE CatalogScolar
GO
USE CatalogScolar


drop table Elev_Adresa 
drop table Elev_Parinte
DROP TABLE Absenta
drop table Nota
drop table MateriePredata
drop table Parinte_Adresa 
DROP TABLE Adresa
drop table Elev 
drop table Clasa
drop table Materie 
drop table Parinte
drop table Profesor


IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'Clasa')
BEGIN
	CREATE TABLE Clasa(
		IDClasa SMALLINT IDENTITY(1,1) PRIMARY KEY,
		Nume NVARCHAR(30) NOT NULL UNIQUE, 
		Etaj SMALLINT NOT NULL,
	);
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'Elev')
BEGIN
	CREATE TABLE Elev(
		IDElev SMALLINT IDENTITY(1,1) PRIMARY KEY,
		Prenume NVARCHAR(50) NOT NULL,
		Nume NVARCHAR(50) NOT NULL, 
		Gen NVARCHAR(10) NOT NULL,
		DataNasterii DATE NOT NULL,
		nrTel CHAR(10) NOT NULL, 
		Email NVARCHAR(50),
		IDClasa SMALLINT,
		CONSTRAINT fk_Elev_IDClasa foreign key(IDClasa) references Clasa(IDClasa) ON UPDATE CASCADE ON DELETE NO ACTION,
		unique(Nume, Prenume)
	);
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'Parinte')
BEGIN
	CREATE TABLE Parinte(
		IDParinte SMALLINT IDENTITY(1,1) PRIMARY KEY,
		Prenume NVARCHAR(50) NOT NULL,
		Nume NVARCHAR(50) NOT NULL, 
		nrTel CHAR(10) NOT NULL, 
		Email NVARCHAR(50),
	);
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'Elev_Parinte')
BEGIN
	CREATE TABLE Elev_Parinte(
		IDElev SMALLINT,
		IDParinte SMALLINT,

		CONSTRAINT fk_Elev_Parinte_IDElev foreign key(IDElev) references Elev(IDElev),
		CONSTRAINT fk_Elev_Parinte_IDParinte foreign key(IDParinte) references Parinte(IDParinte),
		PRIMARY KEY(IDElev, IDParinte)
	);
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'Adresa')
BEGIN
	CREATE TABLE Adresa(
		IDAdrs SMALLINT IDENTITY(1,1) PRIMARY KEY,
		Strada NVARCHAR(50) NOT NULL, 
		Numar NVARCHAR(7) NOT NULL, 
		Apartament SMALLINT DEFAULT NULL
	);
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'Elev_Adresa')
BEGIN
	CREATE TABLE Elev_Adresa(
		IDElev SMALLINT,
		IDAdrs SMALLINT,

		CONSTRAINT fk_Elev_Adresa_IDElev foreign key(IDElev) references Elev(IDElev) ON DELETE CASCADE,
		CONSTRAINT fk_Elev_Adresa_IDAdrs foreign key(IDAdrs) references Adresa(IDAdrs),
		PRIMARY KEY(IDElev, IDAdrs)
	);
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'Parinte_Adresa')
BEGIN
	CREATE TABLE Parinte_Adresa(
		IDParinte SMALLINT,
		IDAdrs SMALLINT,

		CONSTRAINT fk_Parinte_Adresa_IDParinte foreign key(IDParinte) references Parinte(IDParinte),
		CONSTRAINT fk_Parinte_Adresa_IDAdrs foreign key(IDAdrs) references Adresa(IDAdrs),
		PRIMARY KEY(IDParinte, IDAdrs)
	);
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'Profesor')
BEGIN
	CREATE TABLE Profesor(
		IDProf SMALLINT IDENTITY(1,1) PRIMARY KEY,
		Prenume NVARCHAR(50) NOT NULL,
		Nume NVARCHAR(50) NOT NULL, 
		Gen NVARCHAR(10) NOT NULL,
		nrTel CHAR(10) NOT NULL, 
		Email NVARCHAR(50) NOT NULL
	);
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'Materie')
BEGIN
	CREATE TABLE Materie(
		IDMaterie SMALLINT IDENTITY(1,1) PRIMARY KEY,
		Nume NVARCHAR(50) NOT NULL,
	);
END

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'MateriePredata')
BEGIN
	CREATE TABLE MateriePredata(
		IDMatPredata SMALLINT IDENTITY(1,1) PRIMARY KEY,
		DataInceput Date NOT NULL,
		DataFinal Date, 
		IDMaterie SMALLINT, 
		IDProf SMALLINT, 

		CONSTRAINT fk_MateriePredata_IDMaterie foreign key(IDMaterie) references Materie(IDMaterie) ON DELETE CASCADE,
		CONSTRAINT fk_MateriePredata_IDProf foreign key(IDProf) references Profesor(IDProf) ON DELETE CASCADE
	);
END

IF NOT EXISTS ( SELECT *FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'Absenta')
BEGIN
	CREATE TABLE Absenta(
		IDAbs SMALLINT IDENTITY(1,1) PRIMARY KEY, 
		DataAbsenta DATE NOT NULL,
		IDElev SMALLINT,
		IDMatPredata SMALLINT,

		CONSTRAINT fk_Absenta_IDElev foreign key (IDElev) references Elev(IDElev) ON DELETE CASCADE, 
		CONSTRAINT fk_Absenta_IDMatPredata foreign key (IDMatPredata) references MateriePredata(IDMatPredata) 
	);
END

IF NOT EXISTS ( SELECT *FROM INFORMATION_SCHEMA.TABLES where TABLE_NAME = N'Nota')
BEGIN
	CREATE TABLE Nota(
		IDNota SMALLINT IDENTITY(1,1) PRIMARY KEY, 
		DataNota DATE NOT NULL,
		ValoareNota SMALLINT NOT NULL,
		IDElev SMALLINT,
		IDMatPredata SMALLINT,
		CONSTRAINT fk_Nota_IDElev foreign key (IDElev) references Elev(IDElev) ON DELETE CASCADE, 
		CONSTRAINT fk_Nota_IDMatPredata foreign key (IDMatPredata) references MateriePredata(IDMatPredata) ON DELETE CASCADE 
	);
END

